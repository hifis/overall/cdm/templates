#!/bin/bash

# call that script from within the logo folder in question.

rm -v ./*.png

for i in ./*.svg; do
    for dpi in 300 96 24; do
    {
        i_out="${i%.svg}"_${dpi}dpi.png
        inkscape "$i" --export-filename="$i_out" --export-dpi=$dpi
    } &
    done
done
