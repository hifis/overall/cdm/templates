# HIFIS Templates, Logos, Print Materials

This repository contains various printable materials related to HIFIS, like
flyers, posters or handouts.

## Logos
* [HIFIS](logos/HIFIS/)
* [Helmholtz initiative / platform](logos/non_HIFIS/)
* [Helmholtz centres](logos/Helmholtz-centres/)
* [Third party / non-Helmholtz](logos/non_Helmholtz/)

## Poster
* [HIFIS poster](poster/)

## Fonts
* [Hermann font](fonts/hermann/)

## LaTeX Beamer template
* [Github repo](https://github.com/Helmholtz-AI-Energy/beamer-template), clone it and add `\usepackage{hifis}` in the document.

# Other sources on nubes

* [pptx template](https://nubes.helmholtz-berlin.de/f/145883542)
* [Graphical templates](https://nubes.helmholtz-berlin.de/f/100096111)

## Disclaimer
Unless otherwise stated, the contents of this repository including, but not limited to, the images, logos and fonts contained herein are proprietary. However, HIFIS logo and its derivatives can be reused under the [CC BY 4.0 licence](https://creativecommons.org/licenses/by/4.0/legalcode).

> ### PLEASE NOTE 
> Files may be in the inkscape SVG format, which is a superset of regular SVG. 
> This is done to preserve document-specific settings, like guiding lines, document grids and the like.
> If a file is not plain SVG this will be indicated by the ending `.inkscape.svg`.

