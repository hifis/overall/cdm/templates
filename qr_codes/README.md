## Overview

### How to generate a Qr code
You can use https://www.the-qrcode-generator.com/ or https://goqr.me/

### Where do the QR codes lead?
* HIFIS_net_Sticker_qr.svg → https://hifis.net
* HIFIS_net_Sticker_qr_large.svg → https://hifis.net
* hifis-pr_qrcode.svg → https://hifis.net/pr, redirects to https://hifis.net/media
* hifis-newsletter-subscribe.svg → to (un)subscribe to the newsletter: https://lists.desy.de/sympa/subscribe/hifis-newsletter 
